#!/bin/bash

region=$1
efsid=$2
s3bucket=$3

sudo yum update -y
sudo yum install -y nfs-utils

PATH=$PATH:/usr/local/bin
EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`
DIR_TGT=/mnt/efs
DIR_SRC=$EC2_AVAIL_ZONE.$efsid.efs.$region.amazonaws.com
now=$(date +"%Y%m%d")
backupname="efs-$efsid-$now.zip"

sudo mkdir $DIR_TGT
sudo mount -t nfs4 $DIR_SRC:/ $DIR_TGT >> /home/ec2-user/echo.res

cd $DIR_TGT
zip -qr $backupname *
aws s3 cp $backupname s3://$s3bucket/backup/$backupname --region $region
rm $backupname
